<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" 
	  href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
      crossorigin="anonymous">


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body class="text-center">
	
	<h1 class="mb-5 mt-3">Taller Web 1</h1>
	<c:choose>	
		<c:when test="${error == '' }">
			<h4>EL resultado de ${operacion} sobre ${cadena} es : ${resultado}</h4>
		</c:when>
		<c:otherwise>
			<h4>No existe la operacion ${ operacion } </h4>
		</c:otherwise>
	</c:choose>
</body>
</html>