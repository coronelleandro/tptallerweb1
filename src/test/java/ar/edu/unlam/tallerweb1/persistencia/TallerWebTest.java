package ar.edu.unlam.tallerweb1.persistencia;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlam.tallerweb1.SpringTest;
import ar.edu.unlam.tallerweb1.modelo.Ciudad;
import ar.edu.unlam.tallerweb1.modelo.Continente;
import ar.edu.unlam.tallerweb1.modelo.Pais;
import ar.edu.unlam.tallerweb1.modelo.Ubicacion;

public class TallerWebTest extends SpringTest {
	
	//2- Hacer con junit un test que busque todos los pa�ses de habla inglesa.
		@Test @Transactional @Rollback
		public void testQueBuscoPaisesQueHablanIngles(){
		
			Session session = getSession();
					
			//pais 1
			Continente america = new Continente();
			america.setNombre("America");
			
			Pais argentina =new Pais();
			argentina.setNombre("Argentina");
			argentina.setHabitantes((long) 40000000);
			argentina.setIdioma("espa�ol");
			argentina.setContinente(america);
			
			Ubicacion ubicacionBsAs = new Ubicacion();
			ubicacionBsAs.setLatitud(-34.6083);
			ubicacionBsAs.setLongitud(-58.3712);
			
			Ciudad buenosAires=new Ciudad();
			buenosAires.setNombre("Buenos Aires");
			buenosAires.setUbicacionGeografica(ubicacionBsAs);
			
			buenosAires.setPais(argentina);
			session.save(buenosAires);
			argentina.setCapital(buenosAires);
			session.save(argentina);
			
			//pais 2
			Pais estadosUnidos = new Pais();
			estadosUnidos.setNombre("Estados Unidos");
			estadosUnidos.setHabitantes((long) 328835763);
			estadosUnidos.setIdioma("ingles");
			estadosUnidos.setContinente(america);
			
			Ubicacion ubicacionW = new Ubicacion();
			ubicacionW.setLatitud(38.9041);
			ubicacionW.setLongitud(-77.0171);
			
			Ciudad washington = new Ciudad();
			washington.setNombre("Washington");
			washington.setUbicacionGeografica(ubicacionW);
			
			washington.setPais(estadosUnidos);
			session.save(washington);
			estadosUnidos.setCapital(washington);
			session.save(estadosUnidos);
			
			
			//pais 3
			Continente europa = new Continente();
			europa.setNombre("Europa");
			
			Pais inglaterra = new Pais();
			inglaterra.setContinente(europa);
			inglaterra.setIdioma("ingles");
			inglaterra.setNombre("Inglaterra");
			inglaterra.setHabitantes((long) 53012000);
			
			Ubicacion ubicacionL = new Ubicacion();
			ubicacionL.setLatitud(51.5072);
			ubicacionL.setLongitud(-0.1275);
			
			Ciudad londres = new Ciudad();
			londres.setNombre("Londres");
			londres.setUbicacionGeografica(ubicacionL);
			
			londres.setPais(inglaterra);
			session.save(londres);
			inglaterra.setCapital(londres);
			session.save(inglaterra);		
			
			
		
			
			List<Pais> lista=getSession().createCriteria(Pais.class)
					.add(Restrictions.eq("idioma", "ingles"))
					.list();
			assertThat(lista).hasSize(2);
		}
		
		
//Punto 3: - Hacer con junit un test que busque todos los pa�ses del continente europeo.

		@Test @Transactional @Rollback
		public void testQueBuscoPaisesDeEuropa(){
			
			Session session = getSession();
			
			//continentes
			Continente America = new Continente();
			America.setNombre("America");
			session.save(America);
			
			Continente Europa = new Continente();
			Europa.setNombre("Europa");
			session.save(Europa);
			
			
			//paises
			Pais Argentina = new Pais();
			Argentina.setHabitantes( (long) 40000000);
			Argentina.setNombre("Argentina");
			Argentina.setIdioma("espa�ol");
			Argentina.setContinente(America);
			
			Pais Inglaterra = new Pais();
			Inglaterra.setNombre("Inglaterra");
			Inglaterra.setIdioma("ingles");
			Inglaterra.setHabitantes( (long)5000000);
			Inglaterra.setContinente(Europa);
			
	 		Pais Holanda = new Pais();
	 		Holanda.setNombre("Holanda");
	 		Holanda.setHabitantes( (long) 17200000);
	 		Holanda.setIdioma("holandes");
	 		Holanda.setContinente(Europa);	
			
	 		//ubicaciones
	 		Ubicacion ubicacionBsAs = new Ubicacion();
			ubicacionBsAs.setLatitud(-34.6083);
			ubicacionBsAs.setLongitud(-58.3712);
			
			
			Ubicacion ubicacionLondres = new Ubicacion();
			ubicacionLondres.setLatitud(51.5072);
			ubicacionLondres.setLongitud(-0.1275);
			
			Ubicacion ubicacionAms = new Ubicacion();
			ubicacionAms.setLatitud(52.3738);
			ubicacionAms.setLongitud(4.89093);
			
			//ciudades
			Ciudad buenosAires=new Ciudad();
			buenosAires.setNombre("Buenos Aires");
			buenosAires.setUbicacionGeografica(ubicacionBsAs);
			
			Ciudad Londres=new Ciudad();
			Londres.setNombre("Londres");
			Londres.setUbicacionGeografica(ubicacionLondres);
			
			Ciudad Amsterdam = new Ciudad();
			Amsterdam.setNombre("Amsterdam");
			Amsterdam.setUbicacionGeografica(ubicacionAms);
			
			buenosAires.setPais(Argentina);
			session.save(buenosAires);
			Argentina.setCapital(buenosAires);
			session.save(Argentina);
	 		
			Londres.setPais(Inglaterra);
			session.save(Londres);
			Inglaterra.setCapital(Londres);
			session.save(Inglaterra);
			
			Amsterdam.setPais(Holanda);
			session.save(Amsterdam);
			Holanda.setCapital(Amsterdam);
			session.save(Holanda);
	 		
			List<Pais> lista = getSession().createCriteria(Pais.class)
					.add(Restrictions.eq("continente",Europa))
					.list();
			
				assertThat(lista).hasSize(2);
			
			
		}		
		
		//4- Hacer con junit un test que busque todos los pa�ses cuya capital est�n al norte del tr�pico de c�ncer.
		
		@Test @Transactional @Rollback
		public void testQueBuscoPaisesCapitalNorteCancer(){
			
			Session session = getSession();
			
			//continentes
			Continente America = new Continente();
			America.setNombre("America");
			session.save(America);
			
			Continente Europa = new Continente();
			Europa.setNombre("Europa");
			session.save(Europa);
			
			
			//paises
			Pais Argentina = new Pais();
			Argentina.setHabitantes( (long) 40000000);
			Argentina.setNombre("Argentina");
			Argentina.setIdioma("espa�ol");
			Argentina.setContinente(America);
			
			Pais Inglaterra = new Pais();
			Inglaterra.setNombre("Inglaterra");
			Inglaterra.setIdioma("ingles");
			Inglaterra.setHabitantes( (long)5000000);
			Inglaterra.setContinente(Europa);
			
	 		Pais Holanda = new Pais();
	 		Holanda.setNombre("Holanda");
	 		Holanda.setHabitantes( (long) 17200000);
	 		Holanda.setIdioma("holandes");
	 		Holanda.setContinente(Europa);	
			
	 		//ubicaciones
	 		Ubicacion ubicacionBsAs = new Ubicacion();
			ubicacionBsAs.setLatitud(-34.6083);
			ubicacionBsAs.setLongitud(-58.3712);
			
			
			Ubicacion ubicacionLondres = new Ubicacion();
			ubicacionLondres.setLatitud(51.5072);
			ubicacionLondres.setLongitud(-0.1275);
			
			Ubicacion ubicacionAms = new Ubicacion();
			ubicacionAms.setLatitud(52.3738);
			ubicacionAms.setLongitud(4.89093);
			
			//ciudades
			Ciudad buenosAires=new Ciudad();
			buenosAires.setNombre("Buenos Aires");
			buenosAires.setUbicacionGeografica(ubicacionBsAs);
			
			Ciudad Londres=new Ciudad();
			Londres.setNombre("Londres");
			Londres.setUbicacionGeografica(ubicacionLondres);
			
			Ciudad Amsterdam = new Ciudad();
			Amsterdam.setNombre("Amsterdam");
			Amsterdam.setUbicacionGeografica(ubicacionAms);
			
			buenosAires.setPais(Argentina);
			session.save(buenosAires);
			Argentina.setCapital(buenosAires);
			session.save(Argentina);
	 		
			Londres.setPais(Inglaterra);
			session.save(Londres);
			Inglaterra.setCapital(Londres);
			session.save(Inglaterra);
			
			Amsterdam.setPais(Holanda);
			session.save(Amsterdam);
			Holanda.setCapital(Amsterdam);
			session.save(Holanda);
	 		
			List<Pais> lista = getSession().createCriteria(Pais.class)
					.createAlias("capital","capital")
					.createAlias("capital.ubicacionGeografica","ubicacionTotal")
					.add(Restrictions.ge("ubicacionTotal.latitud", 23.5))
					.list();
			
				assertThat(lista).hasSize(2);
		}
		
		//5- Hacer con junit un test que busque todas las ciudades del hemisferio sur
		@Test @Transactional @Rollback
		public void testQueBuscoCiudadesDelHemisferioSur(){
			
			Session session = getSession();
					
			//pais 1
			Continente america = new Continente();
			america.setNombre("America");
			
			Pais argentina =new Pais();
			argentina.setNombre("Argentina");
			argentina.setHabitantes((long) 40000000);
			argentina.setIdioma("espa�ol");
			argentina.setContinente(america);
			
			Ubicacion ubicacionBsAs = new Ubicacion();
			ubicacionBsAs.setLatitud(-34.6083);
			ubicacionBsAs.setLongitud(-58.3712);
			
			
			Ubicacion ubicacionSantiago = new Ubicacion();
			ubicacionSantiago.setLatitud(-29.4110);
			ubicacionSantiago.setLongitud(66.8506);
			Ciudad santiago = new Ciudad();
			
			santiago.setNombre("Santiago");
			santiago.setPais(argentina);
			santiago.setUbicacionGeografica(ubicacionSantiago);
			
			session.save(santiago);
			
			Ciudad buenosAires=new Ciudad();
			buenosAires.setNombre("Buenos Aires");
			buenosAires.setUbicacionGeografica(ubicacionBsAs);
			
			
			
			buenosAires.setPais(argentina);
			session.save(buenosAires);
			argentina.setCapital(buenosAires);
			session.save(argentina);
			
			//pais 2
			Pais estadosUnidos = new Pais();
			estadosUnidos.setNombre("Estados Unidos");
			estadosUnidos.setHabitantes((long) 328835763);
			estadosUnidos.setIdioma("ingles");
			estadosUnidos.setContinente(america);
			
			Ubicacion ubicacionW = new Ubicacion();
			ubicacionW.setLatitud(38.9041);
			ubicacionW.setLongitud(-77.0171);
			
			Ciudad washington = new Ciudad();
			washington.setNombre("Washington");
			washington.setUbicacionGeografica(ubicacionW);
			
			washington.setPais(estadosUnidos);
			session.save(washington);
			estadosUnidos.setCapital(washington);
			session.save(estadosUnidos);
			
			
			//pais 3
			Continente europa = new Continente();
			europa.setNombre("Europa");
			
			Pais inglaterra = new Pais();
			inglaterra.setContinente(europa);
			inglaterra.setIdioma("ingles");
			inglaterra.setNombre("Inglaterra");
			inglaterra.setHabitantes((long) 53012000);
			
			Ubicacion ubicacionL = new Ubicacion();
			ubicacionL.setLatitud(51.5072);
			ubicacionL.setLongitud(-0.1275);
			
			Ciudad londres = new Ciudad();
			londres.setNombre("Londres");
			londres.setUbicacionGeografica(ubicacionL);
			
			londres.setPais(inglaterra);
			session.save(londres);
			inglaterra.setCapital(londres);
			session.save(inglaterra);		
			
			
			List<Ciudad> lista=getSession().createCriteria(Ciudad.class)
					.createAlias("ubicacionGeografica","ubicacionSur")
					.add(Restrictions.lt("ubicacionSur.latitud", 0.00))
					.list();
			assertThat(lista).hasSize(2);
		}
		
}
